package com.arturgomes.chat;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class DAO {
    private DBWrapper dbWrapper;
    private String[] MESSAGE_TABLE_COLUMNS = {"id", "nickname", "content", "isimage"};
    private SQLiteDatabase database;

    public DAO(Context context) {
        dbWrapper = new DBWrapper(context);
    }

    public void open() throws SQLException {
        database = dbWrapper.getWritableDatabase();
    }

    public void close() {
        dbWrapper.close();
    }

    public Message addMessage(String nickname, String text, int isImage) {
        ContentValues values = new ContentValues();
        values.put("nickname", nickname);
        values.put("content", text);
        values.put("isimage", isImage);

        long messageID = database.insert("message", null, values);

        Cursor cursor = database.query("message", MESSAGE_TABLE_COLUMNS,
                "id = " + messageID, null,null,null,null);

        cursor.moveToFirst();

        Message message = parseMessage(cursor);
        cursor.close();
        return message;
    }

    public void deleteMessage(Message message) {
        String id = message.getId();
        database.delete("message", "id = " + id, null);
    }

    public List getAllMessages() {
        List messages = new ArrayList<Message>();
        Cursor cursor = database.query("message", MESSAGE_TABLE_COLUMNS,
                null, null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            Message message = parseMessage(cursor);
            messages.add(message);
            cursor.moveToNext();
        }

        cursor.close();
        return messages;
    }

    public Message getMessage(String id) {
        Cursor cursor = database.query("message", MESSAGE_TABLE_COLUMNS,
                null, null, null, null, null);
        cursor.moveToPosition(Integer.parseInt(id));
        Message message = parseMessage(cursor);
        cursor.close();
        return message;
    }


    public long getTableSize() {
        long count = DatabaseUtils.queryNumEntries(database, "message");
        return count;
    }

    private Message parseMessage(Cursor cursor) {
        Message message = new Message();
        message.setNick(cursor.getString(0));
        message.setIsImage(cursor.getInt(1));
        message.setContent(cursor.getString(2));
        return message;
    }
}
