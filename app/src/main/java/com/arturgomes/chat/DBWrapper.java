package com.arturgomes.chat;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBWrapper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "chat2.db";
    private static final int DATABASE_VERSION = 1;

    private static final String CREATE_DATABASE = "CREATE TABLE message(" +
            "id integer primary key autoincrement, "    +
            "nickname text not null, "                  +
            "content text, "                               +
            "isimage integer not null);"                  ;

    public DBWrapper (Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_DATABASE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS message");
        onCreate(db);
    }
}
