package com.arturgomes.chat;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import static com.arturgomes.chat.MainActivity.nickname;
import static com.arturgomes.chat.MainActivity.serverip;


public class ChatFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private ListView listView;
    private String mParam1;
    private String mParam2;
    private EditText mInputMessageView;
    private RecyclerView mMessagesView;
    private RecyclerView mNickView;
    private OnFragmentInteractionListener mListener;
    private List<Message> mMessages = new ArrayList<Message>();
    private RecyclerView.Adapter mAdapter;
    ArrayList<Bitmap> bitmap_array = new ArrayList<Bitmap>();
    public  Socket socket;
    DatabaseHelper db;

    private List<Message> messagelist;

    public static ChatFragment newInstance(String param1, String param2) {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public ChatFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        db = new DatabaseHelper(getActivity());
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String value = prefs.getString("serverip", "0.0.0.0");
        if (value == "0.0.0.0"){
            showChangeLangDialog();
        }else {
            nickname = prefs.getString("nickname", "nick");
            try{
                socket = IO.socket(value);
                socket.connect();
                socket.on("message", handleIncomingMessages);
            }catch(URISyntaxException e){
                Toast.makeText(getActivity(), "Não foi possível conectar no ip: " + serverip, Toast.LENGTH_SHORT).show();
            }
        }

    }
    public void showChangeLangDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable (false);
        final EditText edt = (EditText) dialogView.findViewById(R.id.edit1);
        final EditText edt2 = (EditText) dialogView.findViewById(R.id.edit2);
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String value1 = prefs.getString("nickname", "nick");
        String value = prefs.getString("serverip", "0.0.0.0");
        edt.setText(value1);
        edt2.setText(value);
        dialogBuilder.setTitle("Configurações");
        dialogBuilder.setMessage("Servidor e Nickname");
        dialogBuilder.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                nickname = edt.getText().toString();
                serverip = edt2.getText().toString();
                edt.setText(nickname);
                edt2.setText(serverip);
                prefs.edit().putString("nickname", nickname).apply();
                prefs.edit().putString("serverip", serverip).apply();
                try{
                    socket = IO.socket(serverip);
                    socket.connect();
                    socket.on("message", handleIncomingMessages);
                }catch(URISyntaxException e){
                    Toast.makeText(getActivity(), "Não foi possível conectar no ip: " + serverip, Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Toast.makeText(getActivity(), "É obrigatório configurar o app.", Toast.LENGTH_SHORT).show();
                System.exit(1);
            }
        });

        AlertDialog b = dialogBuilder.create();
        b.show();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chat, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mAdapter = new MessageAdapter( mMessages);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMessagesView = (RecyclerView) view.findViewById(R.id.messages);
        mMessagesView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mMessagesView.setAdapter(mAdapter);


        ImageButton sendButton = (ImageButton) view.findViewById(R.id.send_button);
        mInputMessageView = (EditText) view.findViewById(R.id.message_input);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = mInputMessageView.getText().toString().trim();
                if(message.equals("")){
                    Toast.makeText(getActivity(), "Empty message", Toast.LENGTH_SHORT).show();
                }
                else {
                    sendMessage();
                }
            }
        });
        mMessagesView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), mMessagesView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        Message message = mMessages.get(position);
                        ImageView i = new ImageView(getActivity());
                        i.setImageBitmap(message.getImage());

                        Toast toastView = new Toast(getActivity());
                        toastView.setView(i);
                        toastView.setDuration(Toast.LENGTH_LONG);
                        toastView.setGravity(Gravity.CENTER, 0, 0);
                        toastView.show();
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );

    }

    private void sendMessage(){
        String message = mInputMessageView.getText().toString().trim();

        mInputMessageView.setText("");
        addMessage(message, nickname);
        JSONObject sendText = new JSONObject();
        try{
            sendText.put("content",message);
            sendText.put("nick",nickname);
            sendText.put("isImage",0);
            socket.emit("message", sendText);
        }catch(JSONException e){

        }
    }

    public void sendImage(String path)
    {
        JSONObject sendData = new JSONObject();
        try{
            sendData.put("content", encodeImage(path));
            Bitmap bmp = decodeImage(sendData.getString("content"));
            addImage(bmp, nickname,encodeImage(path) );
            sendData.put("nick",nickname);
            sendData.put("isImage",1);
            socket.emit("message",sendData);
        }catch(JSONException e){

        }
    }

    private void addMessage(String message, String nick) {
        //dao.addMessage(nick, message, 0);
        //AddData(nick, message, 0);
        mMessages.add(new Message.Builder(Message.TYPE_MESSAGE)
                .message(message, nick).build());
        // mAdapter = new MessageAdapter(mMessages);
        mAdapter = new MessageAdapter( mMessages);
        mAdapter.notifyItemInserted(0);
        scrollToBottom();
    }

    private void addImage(Bitmap bmp, String nick, String encoded){
        //dao.addMessage(nick, encoded, 1);
        //AddData(nick, encoded, 1);
        mMessages.add(new Message.Builder(Message.TYPE_MESSAGE)
                .image(bmp, nick).build());

        mAdapter = new MessageAdapter( mMessages);
        mAdapter.notifyItemInserted(0);
        scrollToBottom();
    }
    private void scrollToBottom() {
        mMessagesView.scrollToPosition(mAdapter.getItemCount() - 1);
    }

    private String encodeImage(String path)
    {
        File imagefile = new File(path);
        FileInputStream fis = null;
        try{
            fis = new FileInputStream(imagefile);
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }
        Bitmap bm = BitmapFactory.decodeStream(fis);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG,100,baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);
        //Base64.de
        return encImage;

    }
    public void AddData(String nickname, String content, int isImage) {

        boolean insertData = db.addData(nickname,content,isImage );

        if(insertData==true){
            Toast.makeText(getActivity(), "Data Successfully Inserted!", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(getActivity(), "Something went wrong :(.", Toast.LENGTH_LONG).show();
        }
    }
    private Bitmap decodeImage(String data)
    {
        byte[] b = Base64.decode(data,Base64.DEFAULT);
        Bitmap bmp = BitmapFactory.decodeByteArray(b,0,b.length);
        return bmp;
    }
    public Emitter.Listener handleIncomingMessages = new Emitter.Listener(){
        @Override
        public void call(final Object... args){
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String nick;
                    String message;
                    int isimage;
                    try {
                        message = data.getString("content").toString();
                        nick = data.getString("nick").toString();
                        if (data.has("isImage")) {
                            isimage = data.getInt("isImage");
                            if (isimage == 0){
                                addMessage(message, nick);
                            } else if (isimage == 1){
                                addImage(decodeImage(message), nick, message);
                            }
                        }else{
                            addMessage(message, nick);
                        }

                    } catch (JSONException e) {

                    }
                }
            });
        }
    };

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        socket.disconnect();
    }

}
