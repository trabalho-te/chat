package com.arturgomes.chat;

import android.graphics.Bitmap;

/**
 * Created by sreejeshpillai on 10/05/15.
 */
public class Message {

    public static final int TYPE_MESSAGE = 0;
    public static final int TYPE_LOG = 1;
    public static final int TYPE_ACTION = 2;

    private int mType;
    private String mId;
    private String mNick;
    private String mMessage;
    private Bitmap mImage;
    private String mContent;
    private int mIsImage;

    public Message() {
    }

    public int getType() {
        return mType;
    };

    public String getMessage() {
        return mMessage;
    };
    public String getId() {
        return mId;
    }
    public Bitmap getImage() {
        return mImage;
    };

    public String getNick() {
        return mNick;
    };

    public void setId(String id) {
        this.mId = id;
    }

    public void setNick(String nick) {
        this.mNick = nick;
    }

    public void setIsImage(int isImage) {
        this.mIsImage = isImage;
    }

    public void setContent(String content) {
        this.mContent = content;
    }


    public static class Builder {
        private final int mType;
        private Bitmap mImage;
        private String mMessage;
        private String mNick;
        private String mId;

        public Builder(int type) {
            mType = type;
        }

        public Builder image(Bitmap image, String nick) {
            mNick = nick;
            mImage = image;
            mMessage = null;
            return this;
        }

        public Builder message(String message,String nick) {
            mNick = nick;
            mMessage = message;
            mImage = null;
            return this;
        }

        public Message build() {
            Message message = new Message();
            message.mId = mId;
            message.mType = mType;
            message.mImage = mImage;
            message.mMessage = mMessage;
            message.mNick = mNick;
            return message;
        }
    }
}